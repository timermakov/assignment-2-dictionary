%include "colon.inc"
%include "words.inc"
%include "lib.inc"


global _start

section .rodata

%define BUF_SIZE 256

input_key: db "Введите ключ: ", 0
found: db "Значение: ", 0
no_found: db "Данного ключа не существует.", 10, 0
too_long_key: db "Длина строки не должна быть более 255 символов.", 10, 0

section .text
_start:
    mov rdi, input_key
    call print_string

    mov rsi, BUF_SIZE
    sub rsp, BUF_SIZE
    mov rdi, rsp
    call read_word
    test rax, rax
    jz .too_long
    mov rdi, rax
    mov rsi, into
    call find_word
    test rax, rax
    jz .not_found
    add rsp, BUF_SIZE
    add rax, 8

    push rax
    mov rdi, found
    call print_string
    pop rdi
    push rdi
    call string_length
    pop rdi

    inc rax
    add rdi, rax
    call print_string
    call print_newline
    jmp exit

.not_found:
    add rsp, BUF_SIZE
    mov rdi, no_found
    call print_error
    jmp exit

.too_long:
    add rsp, BUF_SIZE
    mov rdi, too_long_key
    call print_error
    jmp exit
