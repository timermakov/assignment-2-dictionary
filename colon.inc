section .text

%define into 0
%macro colon 2
    %%next: dq into
    db %1, 0
    %2:
%define into %%next
%endmacro
