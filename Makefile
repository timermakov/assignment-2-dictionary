ASM_FLAGS=-felf64
ASM=nasm
LD=ld
RM=rm

main.o: main.asm colon.inc words.inc
	$(ASM) $(ASM_FLAGS) main.asm

dict.o: dict.asm
	$(ASM) $(ASM_FLAGS) dict.asm

lib.o: lib.asm
	$(ASM) $(ASM_FLAGS) lib.asm

%.o: %.asm $(ASM) $(ASMFLAGS) -o $@ $<

main: main.o dict.o lib.o
	$(LD) -o main main.o dict.o lib.o

.PHONY: clean
clean:
	$(RM) -rf main main.o dict.o lib.o

.PHONY: run
run: main
	echo "Running main..."
	./main


